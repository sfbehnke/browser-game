var thing = $('#my-block');
thing.css('position', 'relative');

print('hello');
print(thing);

function print(obj) {
	console.log(obj);
}

function move_down() {
	var leftValue = thing.css('top');
	var currentPos = parseInt(leftValue, 10);
	print(currentPos);

	var nextPos = currentPos + 6;
	print(nextPos);
	
	thing.css('top', nextPos + 'px');
}

function move_up() {
	var leftValue = thing.css('top');
	var currentPos = parseInt(leftValue, 10);
	print(currentPos);

	var nextPos = currentPos - 6;
	print(nextPos);
	
	thing.css('top', nextPos + 'px');
}

function update() {
	print('UPDATE LOOP');
	
	print('DOWN');
	for (var i = 0; i++; i < 10) {
		print('ha')
		move_down();
	}

	print('UP');	
	for (var i = 0; i++; i < 10) {
		move_up();
	}
}

setInterval(update, 30);